//---------------Adição----------------------

function add(a, b){
    let resSoma = a + b
    
    return resSoma
}
console.log(add(1, 2))

//---------Multiplicação----------------------

function multiply(multiplicando, multiplicador){
    let resMult = 0
    
    for(let i = 1; i <= multiplicador; i++){
        resMult += add(multiplicando, 0)
    }

    return resMult
}
console.log(multiply(3, 4))

//---------Potencição-------------------------

function power(x, n){
    let resPower = 1
    
    for(let i = 1; i <= n; i++){
        resPower = multiply(resPower, x)
    }

    return resPower
 }
 console.log(power(5, 6))

 //-----------Fatorial------------------------
 
 function factorial(f){
     let resFact = 1

     for(let i = 1; i <= f; i++){
         resFact = multiply(resFact, i)
     }

     return resFact
 }
 console.log(factorial(7))

 //-----------Fibonnaci------------------------

 function fibonnaci(n){
    let fib = document.getElementById('fib').value
    if(fib == ''){
        alert('PRIMEIRO PREENCHA O CAMPO NÚMERICO!')
    }else{
        let penultimo = 0
        let ultimo = 1
        let proximo = 0

        if(n <= 2){
            proximo = n - 1
        }else{
            for(let i = 3; i <= n; i++){
                proximo = ultimo + penultimo
                penultimo = ultimo
                ultimo = proximo
            }
        }
        document.getElementById('res').innerHTML += `O ${fib}° termo é ${proximo}<br>`
    }
    
 }
 
 
As demais funções e seus retornos estão no arquivo script.js. Como não foi especificado se seus resultados deveriam ser visualizados no arquivo HTML e, devido as inconveniencias
de tornar isso possível tendo em vista os critérios determinados para a implementação das funções, tais como  não poder usar operadores ou funções aritméticas incorporadas
(como o operador de multiplicação *), percebi que o foco não era demonstrar ou aplicar tais funções, mas sim implementá-las. 
